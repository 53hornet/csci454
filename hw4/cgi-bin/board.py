#!/usr/bin/env python2.7
import Cookie, os, time
import re
import uuid
import cgi
import cgitb

cgitb.enable()		## allows for debugging errors from the cgi scripts in the browser

from output import *

cookie = Cookie.SimpleCookie() # for writing cookies
form = cgi.FieldStorage() # for reading GET datas

if not Login():
    DisplayLogin()

# if we get here, this is an authorized user, let's print the messages
PrintMessages()

exit(0)
