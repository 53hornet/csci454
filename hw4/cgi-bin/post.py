#!/usr/bin/env python2.7
import Cookie, os, time
import re
import uuid
import cgi
import cgitb

from output import *

cgitb.enable() # allows for debugging errors from the cgi scripts in the browser

cookie = Cookie.SimpleCookie() # for writing cookies
form = cgi.FieldStorage() # for reading POST data

message = form.getvalue('message')

user = Login()
if not user:
    ShowError()

if message == None: # to prevent posting empty messages
    RedirectToBoard()

# csrf protection -- check for csrfToken
csrfToken = form.getvalue('csrfToken')

if csrfToken is None:
    ShowError()

with open('sessions', 'r') as s_file:
    for line in s_file:
        if user in line and not csrfToken in line:
            ShowError()

message = message.replace('\n','\n')
with open('messages','a') as m:
    m.write(user + ': ' + message + '\n')

RedirectToBoard()
