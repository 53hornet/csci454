import os
import re

def DisplayLogin():
    print 'Content-Type: text/html\n'
    print '<html><body>'
    print """
    <div id="container">
    <form action="login.py" method="get">
    <label for="username">Username:</label>
    <input type="text" id="username" name="username">
    <label for="password">Password:</label>
    <input type="password" id="password" name="password">
    <div id="lower">
    <input type="submit" value="Login">
    </div><!--/ lower-->
    </form>
    </div>
    """
    print '</body></html>'
    exit(0)

def PrintMessages():
    print 'Content-Type: text/html\n'
    print '<html><body>'
    
    with open("messages", "r") as m_file:
        s = m_file.read()
        l = s.split('\n')
        for i in l:
            if len(i) == 0:
                continue
            i.replace('\\n','\n')
            print i
            print '<br><br>'
        print """
        <br><br>
        <form action="post.py" method="post" style="display:inline">
        <input type="hidden" name="csrfToken" value="""

        # csrf protection -- session token sendoff
        s_id = os.environ.get('HTTP_COOKIE').split('=')[1] # for reading cookies

        with open('sessions', 'r') as s_file:
            for line in s_file:
                if s_id in line:
                    print line.split()[2]

        print """
        <label for="message">Message:</label><br>
        <textarea rows="4" cols="50" name="message"></textarea>
        <br>
        <input type="submit" value="Post"></form>
        """
        print """
        <form action="logout.py" style="display:inline">
            <input type="submit" value="Logout" />
        </form>
        """

    print '</body></html>'

def ShowError():
    print 'Content-Type: text/html\n'
    print '<html><body>'
    print '<h2> Error ocured :P </h2>'
    print '</body></html>'
    exit(0)

def Login():
    cookie_string = os.environ.get('HTTP_COOKIE') # for reading cookies
    g = re.search('session_id=(\w+)', cookie_string) # if g==None -- no cookie
    if not g:
        return False
    with open('sessions', 'r') as s_file:
        s = s_file.read()
        sid = g.group(1)
        g = re.search(sid + ' ' + '(\w+)', s)
        if not g:
            return False
    return g.group(1)

def RedirectToBoard():
    #go back to board.py
    print 'Content-Type: text/html\n'
    print '<meta http-equiv="refresh" content="0; url=board.py" />'
    exit(0)

def RemoveAllUserSessions(user):
    tmp = ''
    f = open('sessions', 'r')
    for line in f:
        if user not in line:
            tmp += line
    f.close()
    with open('sessions','w') as f:
        f.write(tmp)
