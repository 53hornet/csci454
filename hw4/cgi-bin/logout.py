#!/usr/bin/env python2.7
import Cookie, os, time
import re
import uuid
import cgi
import cgitb

from output import *

cgitb.enable()		## allows for debugging errors from the cgi scripts in the browser

cookie = Cookie.SimpleCookie() # for writing cookies
form = cgi.FieldStorage() # for reading GET data

message = form.getvalue('message')

user = Login()
if not user:
    ShowError()
    exit(0)

RemoveAllUserSessions(user)
RedirectToBoard()
