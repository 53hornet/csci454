#!/usr/bin/env python2.7
import Cookie, os, time
import re
import uuid
import cgi
import cgitb
import random

from output import *

cgitb.enable()		## allows for debugging errors from the cgi scripts in the browser

cookie = Cookie.SimpleCookie() # for writing cookies
cookie_string = os.environ.get('HTTP_COOKIE') # for reading cookies
form = cgi.FieldStorage() # for reading GET data

login = form.getvalue('username')
password  = form.getvalue('password')
with open('users', 'r') as users:
    s = users.read()
    if s.find(login + ' ' + password) == -1:
        ShowError()

    # else set session id cookie and store it in the file!
    s_id = uuid.uuid4().hex
    cookie['session_id'] = s_id # login

    # xss protection -- set session_id cookie to httpOnly
    cookie['session_id']['httponly'] = '1'

    with open("sessions", "a") as myfile:
        # csrf protection -- session token construction
        random.seed()
        csrfToken = str(random.random())
        myfile.write(s_id + ' ' + login + ' ' + csrfToken + '\n')

    print cookie

RedirectToBoard()
